using System;

namespace LibIniEntity {
    public class IniException : FormatException {
        public IniException() : base() {}
        public IniException(string msg) : base(msg) {}
        public IniException(string msg, Exception cause) : base(msg, cause) {}
    }
}