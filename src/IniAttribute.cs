using System.Runtime.CompilerServices;
using System;

namespace LibIniEntity {
    public class IniAttribute : Attribute {
        public string Section {get; set;}
        public string Description {get; set;}
        public string Key {get; set;}
        public int Order {get;}

        public IniAttribute([CallerLineNumber]int order = 0) {
            Order = order;
        }
    }
}