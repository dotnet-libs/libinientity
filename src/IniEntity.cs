﻿using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Linq;
using System.IO;
using System;

namespace LibIniEntity {
    public static class IniEntity {
        /// <summary>
        /// Converts the content of an ini file pointed to by
        /// path into an object of type T.
        /// <param name="path">Path to the file to read from</param>
        /// <returns>A new object of type T</returns>
        /// </summary>
        public static T FromFile<T>(string path) where T : class, new() {
            var obj = new T();
            var propMap = new Dictionary<string, PropertyInfo>();
            var commentStarter = path.ToLower().EndsWith(".ini") ? ";" : "#";

            try {
                foreach(var prop in obj
                    .GetType().GetProperties()
                    .Where(prop => prop.GetCustomAttribute<IniAttribute>() != null))
                {
                    var att = prop.GetCustomAttribute<IniAttribute>();
                    propMap.Add(att.Key ?? prop.Name, prop);
                }

                foreach(var line in File.ReadAllLines(path)) {
                    var trimmed = line.Trim();

                    // skip sections, comments and empty lines
                    if(trimmed.StartsWith("[")
                    || trimmed.StartsWith(commentStarter)
                    || trimmed.Length == 0)
                        continue;

                    var separatorIndex = trimmed.IndexOf('=');

                    if(separatorIndex < 0)
                        throw new FormatException("missing assignment operator");

                    var key = trimmed.Substring(0, separatorIndex);
                    var value = trimmed.Substring(separatorIndex + 1, trimmed.Length - separatorIndex - 1);
                    var prop = propMap[key];
                    prop.SetValue(obj, Convert.ChangeType(value, prop.PropertyType));
                }
            } catch(Exception e) {
                if(e is InvalidCastException
                || e is OverflowException
                || e is FormatException
                || e is KeyNotFoundException) {
                    throw new IniException(e.Message, e);
                }

                throw e;
            }

            return obj;
        }

        /// <summary>
        /// Converts an object of type T into ini text and
        /// saves it to the file pointed to by path.
        /// <param name="path">Path to the file to write to</param>
        /// <param name="obj">Object of type T to convert</param>
        /// </summary>
        public static void ToFile<T>(string path, T obj) where T : class {
            var iniText = new StringBuilder();
            var type = obj.GetType();
            var commentStarter = path.EndsWith(".ini") ? ";" : "#";
            var keySet = new HashSet<string>();
            var sectionSet = new HashSet<string>();
            var firstLine = true;

            foreach(var propInfo in type.GetProperties()
                .Where(prop => prop.GetCustomAttribute<IniAttribute>() != null)
                .OrderBy(prop => prop.GetCustomAttribute<IniAttribute>().Order))
            {
                var iniAttr = propInfo.GetCustomAttribute<IniAttribute>();

                // append section
                if(iniAttr.Section != null) {
                    // check if section was already defined
                    if(sectionSet.Contains(iniAttr.Section))
                        throw new IniException("the section '" + iniAttr.Section + "' was already defined");

                    if(!firstLine) {// for style/readability
                        iniText.AppendLine();
                        firstLine = true;
                    }

                    iniText.AppendLine("[" + iniAttr.Section + "]");

                    // add to section set
                    sectionSet.Add(iniAttr.Section);

                    // clear key set to allow equal key identifiers in different sections
                    keySet.Clear();
                }

                // append description
                if(iniAttr.Description != null) {
                    if(!firstLine) // for style/readability
                        iniText.AppendLine();

                    foreach(var commentLine in iniAttr.Description.Split('\n'))
                        iniText.AppendLine(commentStarter + ' ' + commentLine);
                }

                // check read/write access
                if(!propInfo.CanRead)
                    throw new IniException("property has no read access");

                if(!propInfo.CanWrite)
                    throw new IniException("property has no write access");

                var key = iniAttr.Key ?? propInfo.Name;

                // check if key was already defined in this section
                if(keySet.Contains(key))
                    throw new IniException("the key '" + key + "' was already defined in this section");

                // add to key set
                keySet.Add(key);

                var value = propInfo.GetValue(obj);

                // append key/value pair
                iniText.AppendLine(key + "=" + value);
                firstLine = false;
            }

            File.WriteAllText(path, iniText.ToString());
        }
    }
}
