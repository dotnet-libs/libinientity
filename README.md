# LibIniEntity

Your lightweight ini parser.

## Description

Ini files can be parsed from or to objects directly by using the static generic methods `IniEntity<T>.FromIni(string path)` and `IniEntity.ToIni<T>(string path, T obj)`.

Only `public` properties with read and write access and marked with the `IniAttribute` will be parsed.

The `IniAttribute` offers following optional fields:

- `Section` Defines a new section above the property
- `Description` A description which is placed above the property (multi-line descriptions are supported)
- `Key` Redefines the key identifier for this property (if not set the property name is used)

## Additional Notes

Parsing is based on the rules defined [here](https://en.wikipedia.org/wiki/INI_file). Duplicate sections or duplicate keys in a section will cause an `IniException`.

The type of properties marked with `IniAttribute` requires to implement the [`IConvertible`](https://docs.microsoft.com/de-de/dotnet/api/system.iconvertible?view=net-5.0) interface to be able to be parsed.

If the name of the file pointed to by the path passed to `FromIni()` or `ToIni()` ends with an `.ini` extension the *;* symbol is used to determine or mark comment lines otherwise the *#* symbol is used.

Parsing an object created from a partial class is not adviced and may result in unexpected behaviour.
